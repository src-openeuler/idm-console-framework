Name:                idm-console-framework
Summary:             Identity Management Console Framework
URL:                 https://github.com/dogtagpki/idm-console-framework/
License:             LGPLv2
BuildArch:           noarch
Version:             2.1.0
Release:             1
Source:              https://github.com/dogtagpki/idm-console-framework/archive/v%{version}/idm-console-framework-%{version}.tar.gz
BuildRequires:       jss >= 5.0 ldapjdk >= 5.0
BuildRequires:       java-latest-openjdk-devel ant >= 1.6.2
Requires:            java-latest-openjdk-headless jss >= 5.0 ldapjdk >= 5.0
%description
A Java Management Console framework used for remote server management.

%prep
%autosetup -n idm-console-framework-%{version}%{?_phase} -p 1

%build
openjdk_latest_version=`rpm -qi java-latest-openjdk-headless | grep Version | cut -b 15-16`
home_path=`ls /usr/lib/jvm | grep java-${openjdk_latest_version}-openjdk-${openjdk_latest_version}`
export JAVA_HOME=/usr/lib/jvm/${home_path}
ant \
    -Dlib.dir=%{_libdir} \
    -Dbuilt.dir=`pwd`/built \
    -Dclassdest=%{_javadir}

%install
install -d $RPM_BUILD_ROOT%{_javadir}
install -m644 built/release/jars/idm-console-* $RPM_BUILD_ROOT%{_javadir}
pushd %{buildroot}
ln -s %{_javadir}/idm-console-framework.jar %{buildroot}%{_javadir}/idm-console-base.jar
ln -s %{_javadir}/idm-console-framework.jar %{buildroot}%{_javadir}/idm-console-mcc.jar
ln -s %{_javadir}/idm-console-framework.jar %{buildroot}%{_javadir}/idm-console-mcc_en.jar
ln -s %{_javadir}/idm-console-framework.jar %{buildroot}%{_javadir}/idm-console-nmclf.jar
ln -s %{_javadir}/idm-console-framework.jar %{buildroot}%{_javadir}/idm-console-nmclf_en.jar
popd


%files
%doc LICENSE
%{_javadir}/idm-console-framework.jar
%{_javadir}/idm-console-base.jar
%{_javadir}/idm-console-mcc.jar
%{_javadir}/idm-console-mcc_en.jar
%{_javadir}/idm-console-nmclf.jar
%{_javadir}/idm-console-nmclf_en.jar

%changelog
* Tue Aug 13 2024 wangkai <13474090681@163.com> - 2.1.0-1
- Update to 2.1.0
- Merge the jar of mcc,nmclf,base to framework

* Tue Nov 29 2022 yaoxin <yaoxin30@h-partners.com> - 2.0.0-3
- Fix the idm-console-framework compilation failure

* Thu Jan 19 2023 caodongxia <caodongxia@h-partners.com> - 2.0.0-2
- Change the JDK version

* Wed Jun 15 2022 liyanan <liyanan32@h-partners.com> - 2.0.0-1
- Update to  2.0.0

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.2.0-5
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Thu Jul 23 2020 yanan li <liyanan032@huawei.com> - 1.2.0-4
- Package init
